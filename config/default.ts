export default {
	port: 1337,
	host: 'localhost',
	dbUri: 'mongodb://localhost:27017/rest-api',
	saltWorkFactor: 10,
	accessTokenTtl: '15m',
	refreshTokenTtl: '1y',
	privateKey: `-----BEGIN RSA PRIVATE KEY-----
	MIICXAIBAAKBgQDWucNKGhBufB0RBvKld0Lij66ZF1zs2bomlHAlL32ZBUIJiUSw
	v+ZaKSgwixAsA/DSG1WyuOvOMvCR36OtnI+PPmdLfyByhQrRH6axAKn5GqIUfJ+K
	QtwoHJah0OzYZIwTKRNYd/Wj79BFKshW/9RiwqD1nyi0BBuT1ye7aKFmHwIDAQAB
	AoGAAJsrovTp4PX2uwqS+saYaL1RF9uKBp0iumMm4eP9bcYnBpHdoMYZ511AYMqz
	s/wa3Oz40/VTrRRVu4OvGZlcimCmwRH5+/eAp4qdE1m5arsZLRVtdmlv2Ia8BMEm
	ZmZyIcqVVEGC2QelTtvUdgmPZJvhC/+OFIEhrO99PCQUfhkCQQDwRwDRZkXA9SMW
	JpqMW34rgz3BLod5sQxxm7a1y9G4joodEIFB4tW3Eq9BMkLys4an9MwfqvpAd86n
	9s8D5dNLAkEA5Ma7I0YD9mzMHAT7Djp6OJb54Aw3gj0l72pUCk9QOciflNbINe4U
	HJjov6FVD0zoLoWV++xYm0LQBtNWgkyf/QJAIBztQA9SLUrWCKTaLP4ha8FbpJEK
	bDo8doagMcXu3qK2VNIa5Gpjs9ccczM7fWJ/Eb2G9oiMb8dTaGF1I4k45wJBAILo
	iNr1TH0s4lrCKtqIcbmCArfGpdi7nsJxyGch3VlDSLWFdYkOk6mNgdltutuHeXBt
	dlZydn1ZbOoK8nya1g0CQFyyVra604XBQgCHnBDPPXv6MGwfoW/Vmo5sMMbm5A4s
	n+Aj/5arAmPVwhDrEC8VC8Jx4brteP7u+yXAA+5SrTY=
	-----END RSA PRIVATE KEY-----`,
	// 35:42
}
