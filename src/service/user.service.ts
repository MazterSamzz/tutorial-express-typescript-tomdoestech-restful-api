import { omit } from 'lodash'
import { DocumentDefinition } from 'mongoose'
import { string } from 'yup/lib/locale'
import User, { UserDocument } from '../model/user.model'

export async function createUser(input: DocumentDefinition<UserDocument>) {
	try {
		return await User.create(input)
	} catch (e) {
		const error = e as string | undefined
		throw new Error(error)
	}
}

function findUser() {}

export async function validatePassword({
	email,
	password,
}: {
	email: UserDocument['email']
	password: string
}) {
	const user = await User.findOne({ email })

	if (!user) {
		return false
	}

	const isValid = await user.comparePassword(password)

	if (!isValid) {
		return false
	}

	return omit(user.toJSON(), 'password') as Omit<UserDocument, 'password'>
}
