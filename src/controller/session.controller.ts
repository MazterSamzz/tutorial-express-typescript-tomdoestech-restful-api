import { Request, Response } from 'express'
import { validatePassword } from '../service/user.service'
import { createAccessToken, createSession } from '../service/session.service'
import config from 'config'
import { sign } from 'jsonwebtoken'

export async function createUserSessionHandler(req: Request, res: Response) {
	// validate the email and password
	const user = await validatePassword(req.body)

	if (!user) {
		return res.status(401).send('Invalid username or password')
	}

	// create a session
	const session = await createSession(
		String(user._id),
		req.get('user-agent') || ''
	)

	// create access token
	const accessToken = createAccessToken({
		user,
		session,
	})

	const refreshToken = sign(session, config.get<string>('privateKey'), {
		expiresIn: config.get('refreshTokenTtl'), // 1 year
	})

	// send refresh & access token back
	return res.send({ accessToken, refreshToken })
}
